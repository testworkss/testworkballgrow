﻿using Zenject;
using System.Collections.Generic;
using UnityEngine;
using OrCor_TestWork.Audio;

namespace OrCor_TestWork.DataInstallers
{

    [CreateAssetMenu(fileName = "SoundChannelsData", menuName = "Installers/SoundChannelsData")]
    public class SoundChannelModelInstaller : ScriptableObjectInstaller<SoundChannelModelInstaller>
    {
        public List<SoundChannelModel> soundChannels;

        public override void InstallBindings()
        {
            Container.BindInstance(soundChannels);
        }
    }
}
