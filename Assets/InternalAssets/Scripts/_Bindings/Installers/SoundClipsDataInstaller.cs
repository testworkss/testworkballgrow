﻿using OrCor_TestWork.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OrCor_TestWork.DataInstallers {

    [CreateAssetMenu(fileName = "SoundClipsDataInstaller", menuName = "Installers/SoundClipsData")]
    public class SoundClipsDataInstaller : ScriptableObjectInstaller<SoundClipsDataInstaller>
    {
        public List<SoundClipsData> audioData;

        public override void InstallBindings()
        {
            Container.BindInstance(audioData);
        }

    }

}