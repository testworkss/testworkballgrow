﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using OrCor_TestWork.Scriptables;

namespace OrCor_TestWork.Player
{

    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField]
        private Settings _settings;

        public override void InstallBindings()
        {
            Container.Bind<PlayerModel>().AsSingle().WithArguments(_settings.Rigidbody, _settings.Transform, _settings.MeshRenderer);

            Container.BindInterfacesTo<PlayerInputHandler>().AsSingle().WithArguments(_settings.moveSettings);
            Container.Bind<PlayerInputState>().AsSingle();
            Container.BindInterfacesTo<PlayerMoveHandler>().AsSingle().WithArguments(_settings.moveSettings);
        }

        [Serializable]
        public class Settings
        {
            public Rigidbody Rigidbody;
            public Transform Transform;
            public MeshRenderer MeshRenderer;
            public MoveSettings moveSettings;
        }
    }

}