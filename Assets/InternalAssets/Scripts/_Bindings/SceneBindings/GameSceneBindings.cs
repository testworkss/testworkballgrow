﻿using OrCor_TestWork;
using OrCor_TestWork.Pool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OrCor_TestWork
{

    public class GameSceneBindings : MonoInstaller
    {

        public override void InstallBindings()
        {
            InitServices();
            InitUiViews();
        }

        private void InitServices()
        {
            Container.BindInterfacesAndSelfTo<GameManager>().AsSingle();
            Container.Bind<LevelBoundary>().AsSingle();
        }

        private void InitUiViews()
        {
            Container.Bind(typeof(IInitializable), typeof(IDisposable), typeof(IUIElement)).To<GamePage>().AsSingle();
        }

    }

}
