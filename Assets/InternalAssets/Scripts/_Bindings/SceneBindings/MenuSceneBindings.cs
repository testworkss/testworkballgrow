﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OrCor_TestWork
{
    public class MenuSceneBindings : MonoInstaller
    {
        public override void InstallBindings()
        {
            InitServices();
            InitUiViews();
        }

        private void InitServices()
        {
            
        }

        private void InitUiViews()
        {
            Container.Bind(typeof(IInitializable), typeof(IDisposable), typeof(IUIElement)).To<MenuPage>().AsSingle();
        }
    }
}
