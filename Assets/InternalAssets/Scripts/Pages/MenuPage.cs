﻿using System;
using UnityEngine;
using UniRx;
using Zenject;
using UniRx.Async;
using OrCor_TestWork.Audio;

namespace OrCor_TestWork
{

    public class MenuPage : IInitializable, IDisposable, IUIElement
    {
        public GameObject SelfPage { get; set; }

        private readonly UIManager _uiManager;
        private readonly LoadObjectsManager _loadObjectsManager;
        private readonly GameStateManager _gameStateManager;
        private readonly SoundSystem _soundSystem;

        private MenuPageComponent _pageComponent;
        private CompositeDisposable _disposables = new CompositeDisposable();
        private UniTask<UnityEngine.Object>.Awaiter _awaiter;

        public MenuPage(UIManager uiManager,
                        LoadObjectsManager loadObjectsManager,
                        GameStateManager gameStateManager,
                        SoundSystem soundSystem)
        {
            _uiManager = uiManager;
            _loadObjectsManager = loadObjectsManager;
            _gameStateManager = gameStateManager;
            _soundSystem = soundSystem;

            _uiManager.AddPage(this);

            _awaiter = _loadObjectsManager.GetObjectByPath(Constants.PATH_TO_UI_PREFABS + "MenuPage").GetAwaiter();

            _awaiter.OnCompleted(() => {
                SelfPage = MonoBehaviour.Instantiate(_awaiter.GetResult() as GameObject);
                SelfPage.transform.SetParent(_uiManager.Canvas.transform, false);
                _pageComponent = SelfPage.GetComponent<MenuPageComponent>();
            });

        }

        public void Initialize()
        {
            _awaiter.OnCompleted(() => {
                _pageComponent.Info.playBtn.onClick.AddListener(PlayBtnClick);

                Hide();
            });
        }

        public void AfterInit()
        {

        }

        public void Hide()
        {
            SelfPage.SetActive(false);
        }

        public void Show()
        {
            SelfPage.SetActive(true);
        }

        public void Dispose()
        {
            _uiManager.RemovePage(this);
            MonoBehaviour.Destroy(SelfPage);
        }

        private void PlayBtnClick()
        {
            _soundSystem.Play(SoundChannelsNames.UiSingle, SoundClips.CLICK, false);
            _gameStateManager.ChangeState(Enumerators.GameStateTypes.START_GAMEPLAY);
        }
    }
}
