﻿using UnityEngine.UI;
using UnityEngine;


namespace OrCor_TestWork
{

    public class MenuPageComponent : MonoBehaviour
    {

        public PanelInfo Info { get { return _info; } }

        [SerializeField] private PanelInfo _info;

        [System.Serializable]
        public struct PanelInfo
        {
            public Button playBtn;
        }
    }
}