﻿using UnityEngine;

namespace OrCor_TestWork
{
    public interface IUIElement
    {
        GameObject SelfPage { get; set; }

        void Show();
        void Hide();
    }
}