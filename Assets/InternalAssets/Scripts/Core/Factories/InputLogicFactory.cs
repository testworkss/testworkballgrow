﻿using OrCor_TestWork.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OrCor_TestWork.Factories
{

    public static class InputLogicFactory
    {
        private static Dictionary<string, Type> _typesData;
        private static bool IsInitialized => _typesData != null;

        private static void InitializeFactory()
        {
            if (IsInitialized) return;

            var moveTypes = Assembly.GetAssembly(typeof(InputHandlerBase)).GetTypes().
                            Where(p => p.IsClass && !p.IsAbstract && p.IsSubclassOf(typeof(InputHandlerBase))).ToList();

            _typesData = new Dictionary<string, Type>();

            foreach (var type in moveTypes)
            {
                var temp = Activator.CreateInstance(type) as InputHandlerBase;
                _typesData.Add(temp.MoveType.ToString(), type);
            }   
        }

        public static InputHandlerBase GetFactory(string getType)
        {
            InitializeFactory();

            if (_typesData.ContainsKey(getType))
            {
                Type type = _typesData[getType];
                var move = Activator.CreateInstance(type) as InputHandlerBase;
                return move;
            }

            return null;
        }
    }
}