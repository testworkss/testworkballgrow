﻿using System;
using Zenject;

namespace OrCor_TestWork
{
    public abstract class GameStateEntity : IInitializable, ITickable, IDisposable
    {

        public abstract void Initialize();

        public abstract void Start();

        public abstract void Tick();

        public abstract void Dispose();

    }
}
