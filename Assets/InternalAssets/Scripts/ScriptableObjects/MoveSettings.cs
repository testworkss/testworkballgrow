﻿using UnityEngine;
using static OrCor_TestWork.Enumerators;

namespace OrCor_TestWork.Scriptables
{
    [CreateAssetMenu(menuName = "DataBase/MoveObjectsData", fileName = "MoveSettingsData")]
    public class MoveSettings : ScriptableObject
    {
        [SerializeField] private float _moveSpeed = 10f;
        [SerializeField] private MoveObjectTypes _moveType;
        [SerializeField] private float _boundaryBuffer;
        [SerializeField] private float _boundaryAdjustForce;

        public float MoveSpeed { get { return _moveSpeed; } }
        public MoveObjectTypes MoveType { get { return _moveType; } }
        public float BoundaryBuffer { get { return _boundaryBuffer; } }
        public float BoundaryAdjustForce { get { return _boundaryAdjustForce; } }
    }
}

