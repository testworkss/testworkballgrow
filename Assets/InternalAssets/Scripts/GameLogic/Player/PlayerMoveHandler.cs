﻿using OrCor_TestWork.Scriptables;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OrCor_TestWork.Player
{

    public class PlayerMoveHandler : IFixedTickable
    {
        private readonly PlayerModel _playerModel;
        private readonly PlayerInputState _inputState;
        private readonly MoveSettings _moveSettings;
        private readonly LevelBoundary _levelBoundary;

        public PlayerMoveHandler(PlayerModel playerModel, 
                                 PlayerInputState inputState, 
                                 MoveSettings moveSettings,
                                 LevelBoundary levelBoundary)
        {
            _playerModel = playerModel;
            _inputState = inputState;
            _moveSettings = moveSettings;
            _levelBoundary = levelBoundary;

            _inputState.OnClickHandler += ChangePlayerPosition;
        }

        public void FixedTick()
        {
            if (_playerModel.IsDead) return;

            // Always ensure we are on the main plane
            _playerModel.Position = new Vector3(_playerModel.Position.x, _playerModel.Position.y, 0);

            KeepPlayerOnScreen();
        }

        private void ChangePlayerPosition()
        {
            var goalDir = _inputState.NextMovePosition - _playerModel.Position;
            goalDir.z = 0;
            goalDir.Normalize();

            _playerModel.Rotation = Quaternion.LookRotation(goalDir) * Quaternion.AngleAxis(90, Vector3.up);

            float forceIncreaser = _inputState.ClickDuration > 2 ? 2 : _inputState.ClickDuration;
            forceIncreaser *= 100;

            _playerModel.AddForce(goalDir * _moveSettings.MoveSpeed * forceIncreaser);
        }

        void KeepPlayerOnScreen()
        {
            var extentLeft = (_levelBoundary.Left + _moveSettings.BoundaryBuffer) - _playerModel.Position.x;
            var extentRight = _playerModel.Position.x - (_levelBoundary.Right - _moveSettings.BoundaryBuffer);

            if (extentLeft > 0)
            {
                _playerModel.AddForce(
                    Vector3.right * _moveSettings.BoundaryAdjustForce * extentLeft);
            }
            else if (extentRight > 0)
            {
                _playerModel.AddForce(
                    Vector3.left * _moveSettings.BoundaryAdjustForce * extentRight);
            }

            var extentTop = _playerModel.Position.y - (_levelBoundary.Top - _moveSettings.BoundaryBuffer);
            var extentBottom = (_levelBoundary.Bottom + _moveSettings.BoundaryBuffer) - _playerModel.Position.y;

            if (extentTop > 0)
            {
                _playerModel.AddForce(
                    Vector3.down * _moveSettings.BoundaryAdjustForce * extentTop);
            }
            else if (extentBottom > 0)
            {
                _playerModel.AddForce(
                    Vector3.up * _moveSettings.BoundaryAdjustForce * extentBottom);
            }
        }

    }

}