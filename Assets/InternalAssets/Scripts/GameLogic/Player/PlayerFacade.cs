﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace OrCor_TestWork.Player
{
    public class PlayerFacade : MonoBehaviour
    {
        private PlayerModel _model;

        [Inject]
        public void Construct(PlayerModel model)
        {
            _model = model;
        }

        public bool IsDead
        {
            get { return _model.IsDead; }
        }

        public Vector3 Position
        {
            get { return _model.Position; }
        }

        public Quaternion Rotation
        {
            get { return _model.Rotation; }
        }
    }

}

