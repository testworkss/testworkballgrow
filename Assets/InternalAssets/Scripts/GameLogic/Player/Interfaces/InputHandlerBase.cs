﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static OrCor_TestWork.Enumerators;

namespace OrCor_TestWork.Player
{

    public abstract class InputHandlerBase
    {
        protected PlayerInputState _playerInputState;
        protected Camera _mainCamera;

        public abstract MoveObjectTypes MoveType { get; }
        public abstract void ReadInput();

        public void Init(PlayerInputState playerInputState, Camera mainCamera)
        {
            _playerInputState = playerInputState;
            _mainCamera = mainCamera;
        }
    }

}