﻿using UnityEngine;
using System.Collections;
using Zenject;
using System;
using OrCor_TestWork.Factories;
using OrCor_TestWork.Scriptables;

namespace OrCor_TestWork.Player
{
    public class PlayerInputHandler : ITickable
    {
        private readonly PlayerInputState _playerInputState;
        private readonly MoveSettings _moveSettings;
        private readonly Camera _mainCamera;
        private readonly InputHandlerBase _moveInput;

        public PlayerInputHandler(PlayerInputState playerInputState, MoveSettings moveSettings, Camera mainCamera)
        {
            _playerInputState = playerInputState;
            _moveSettings = moveSettings;
            _mainCamera = mainCamera;
            _moveInput = InputLogicFactory.GetFactory(_moveSettings.MoveType.ToString());
            _moveInput.Init(_playerInputState, _mainCamera);
        }

        public void Tick()
        {
            _moveInput.ReadInput();
        }
    }

    public class PlayerInputState
    {
        public Action OnClickHandler = delegate { };
        public Vector3 NextMovePosition;
        public float ClickDuration;
    }
}