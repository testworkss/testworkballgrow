﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrCor_TestWork.Player
{

    public class PlayerModel
    {
        private float _health = 100.0f;

        public PlayerModel(
                Rigidbody rigidBody,
                MeshRenderer renderer,
                Transform transform)
        {
            SelfRigidbody = rigidBody;
            Renderer = renderer;
            SelfTransform = transform;
        }

        public MeshRenderer Renderer { get; }
        public Rigidbody SelfRigidbody { get; }
        public Transform SelfTransform { get; }

        public bool IsDead { get; set; }

        public float Health
        {
            get { return _health; }
        }

        public Vector3 LookDir
        {
            get { return -SelfRigidbody.transform.right; }
        }

        public Quaternion Rotation
        {
            get { return SelfRigidbody.rotation; }
            set { SelfRigidbody.rotation = value; }
        }

        public Vector3 Position
        {
            get { return SelfRigidbody.position; }
            set { SelfRigidbody.position = value; }
        }

        public void AddForce(Vector3 force)
        {
            SelfRigidbody.AddForce(force);
        }

    }

}