﻿using CustomLogger;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrCor_TestWork.Player
{

    public class ClickMoveInput : InputHandlerBase
    {
        public override Enumerators.MoveObjectTypes MoveType { get { return Enumerators.MoveObjectTypes.ClickMove; } }

        private float _clickDuration;

        public override void ReadInput()
        {

            if (Input.GetMouseButtonDown(0))
            {
                _clickDuration = 0;
            }
            else if (Input.GetMouseButton(0))
            {
                _clickDuration += Time.deltaTime;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                var mouseRay = _mainCamera.ScreenPointToRay(Input.mousePosition);
                var mousePos = mouseRay.origin;
                mousePos.z = 0;

                _playerInputState.NextMovePosition = mousePos;
                _playerInputState.ClickDuration = _clickDuration;
                _clickDuration = 0;

                _playerInputState.OnClickHandler();
            }
        }
    }

}