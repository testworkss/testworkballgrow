﻿namespace OrCor_TestWork.Audio
{
    public enum SoundChannelsNames
    {
        Music,
        Effects, 
        Dialogs,
        UiSingle,
        UiLoop
    }
}
